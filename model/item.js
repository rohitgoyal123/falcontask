var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var itemSchema = new Schema({
    content: {
        message: String,
        network: String,
        postType: String,
        media: {
            fileName: String,
            url: String
        }
    },
    tags: [String],
    status: String,
    channels: [{
        name: String
    }],
    scheduled: String,
    geo: {
        countries: [{
            value: String,
            key: Number
        }],
        languages: [{
            value: String,
            key: Number
        }],
        cities: [{
            value: String,
            key: Number
        }],
        regions: [{
            value: String,
            key: Number
        }]
    }
});
var Item = mongoose.model("Item", itemSchema);

module.exports = {
    Item: Item
};