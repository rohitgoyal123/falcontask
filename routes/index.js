var express = require('express');
var itemService = require("../services/item-service");
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {
        title: 'Express'
    });
});

/*GET all items list, returns json object list*/
router.get('/items', function(req, res, next) {
    itemService.getItems(function(err, items) {
        if (err)
            res.send(err);
        else
            res.send(items);
    })
});

/*GET an item with id, returns json object*/
router.get('/items/:itemId', function(req, res, next) {
    itemService.getItem(req.params.itemId, function(err, item) {
        if (err)
            res.send(err);
        else
            res.send(item);
    })
});

/*Add a new item with POST body, returns the item object on success or error message*/
router.post('/items', function(req, res, next) {
    itemService.addItem(req.body, function(err,item) {
        if (err) {
            res.send(err);
        } else {
            res.send(item);
        }

    });
});


/*Update an item with id using PUT method, body accepts only updated fields, returns the item object*/
router.put('/items/:itemId', function(req, res, next) {
    itemService.updateItem(req.params.itemId, req.body, function(err, item) {
        if (err)
            res.send(err);
        else
            res.send(item);
    });
});

/*DELETE an item with id, returns "success" or error message*/
router.delete('/items/:itemId', function(req, res, next) {
    itemService.deleteItem(req.params.itemId, function(err) {
        if (err)
            res.send(err);
        else
            res.send("success");
    });
});


module.exports = router;