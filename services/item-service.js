var Item = require("../model/item").Item;

//service to add item object in the database
exports.addItem = function(item, next) {
    var newItem = new Item({
        content: item.content,
        channels: item.channels,
        tags: item.tags,
        status: item.status,
        scheduled: item.scheduled,
        geo: item.geo
    });
    newItem.save(function(err,addedItem) {
        if (err) {
            return next(err,null);
        }
        return next(null,addedItem);
    });
};

//service to get all items from the database
exports.getItems = function(next) {
    Item.find(function(err, items) {
        if (err)
            return next(err, null);
        return next(null, items);
    });
};

//service to get an item with id from the database
exports.getItem = function(id, next) {
    Item.findById({
        '_id': id
    }, function(err, item) {
        if (err)
            return next(err, null);
        return next(null, item);
    });
};

//service to update an item with id in the database
exports.updateItem = function(id, updatedItem, next) {
    Item.findById({
        '_id': id
    }, function(err, item) {
        if (err)
            return next(err, null);
        if (updatedItem.content)
            item.content = updatedItem.content;
        if (updatedItem.tags)
            item.tags = updatedItem.tags;
        if (updatedItem.status)
            item.status = updatedItem.status;
        if (updatedItem.channels)
            item.channels = updatedItem.channels;
        if (updatedItem.scheduled)
            item.scheduled = updatedItem.scheduled;
        if (updatedItem.geo)
            item.geo = updatedItem.geo;
        item.save(function(err,updatedItem){
            if(err)
                return 
        })
        return next(null, item);
    });
};

//service to delete an item with id from the database
exports.deleteItem = function(id, next) {
    Item.findByIdAndRemove({
        '_id': id
    }, function(err) {
        if (err)
            return next(err);
        return next(null);
    });
};