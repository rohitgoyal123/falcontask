'use strict';

var app = angular.module('app');
app.controller('MainController', function($scope, $http) {
    $scope.items = [];
    $scope.configureNewItemObj=function(){
        $scope.newItem={};
        $scope.newItem.channels=[];
        $scope.newItem.geo={};
        $scope.newItem.geo.countries=[];
        $scope.newItem.geo.languages=[];
        $scope.newItem.geo.cities=[];
        $scope.newItem.geo.regions=[];
    }
    
    $scope.configureNewItemObj();
    angular.element(document).ready(function () {
        $http({
            method: 'GET',
            url: "https://falcontask-rohitgoyal123.c9.io/items"
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
            $scope.items = response.data;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.error(response);
        })
    
    });

    $scope.saveItem = function() {
        if ($scope.newItem._id == null) {
            //if this is new item, POST Api
            $http.post('https://falcontask-rohitgoyal123.c9.io/items', $scope.newItem)
            .then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                $scope.items.push(response.data);
                $scope.configureNewItemObj();
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.error(response);
            })
        } else {
            //For existing item, PUT Api
            $http.put('https://falcontask-rohitgoyal123.c9.io/items/' + $scope.newItem._id, $scope.newItem)
            .then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                for (var i in $scope.items) {
                    if ($scope.items[i]._id == $scope.newItem._id) {
                        $scope.items[i] = response.data;
                    }
                }
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.error(response);
            })
        }
    };


    $scope.deleteItem = function(id) {
        $http.delete('https://falcontask-rohitgoyal123.c9.io/items/' + id)
        .then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available
                for (var i in $scope.items) {
                    if ($scope.items[i]._id == id) {
                        $scope.items.splice(i, 1);
                        $scope.configureNewItemObj();
                    }
                }
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                console.error(response);
            })
    };


    $scope.cancelItem = function() {
        $scope.configureNewItemObj();
    }
    
    $scope.editItem = function(id) {
        //search item with given id and update it
        for (var i in $scope.items) {
            if ($scope.items[i]._id == id) {
                //we use angular.copy() method to create 
                //copy of original object
                $scope.newItem = angular.copy($scope.items[i]);
            }
        }
    }
})