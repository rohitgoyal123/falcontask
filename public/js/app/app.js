'use strict';

angular.module('app',['ngRoute'])
        .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/js/app/index.html',
        controller: 'MainController'
      })}]);
        
